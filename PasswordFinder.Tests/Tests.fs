module Tests

open Expecto
open PasswordFinder

[<Tests>]
let tests =
    testList "Password Finder Tests" [

        testCase "Can turn a string of number into a list of numbers" <| fun _ ->
            let nums = toIntArray "3453"
            Expect.equal nums [3;4;5;3] "Oops didn't retrun a string with the numbers in it."
    
        testCase "Prepend 0's to list of given length" <| fun _ ->
            let numList = toIntArray "2453"
            let nums = prepend0s 6 numList
            Expect.equal nums [0;0;2;4;5;3] "0's weren't prepended"
            
        testCase "Applies rule one (no transform): assending or equal values only within the given range" <| fun _ ->
            let initPwd = largestPwd [0;0;2;4;4;9]
            Expect.equal initPwd [0;0;2;4;4;9] "Initial pwd list not correct."
                        
        testCase "Applies rule one (last 2 -> 5;0 => 4;9): assending or equal values only within the given range" <| fun _ ->
            let initPwd = largestPwd [0;0;2;4;5;0]
            Expect.equal initPwd [0;0;2;4;4;9] "Initial pwd list not correct."

        testCase "Applies rule one (last 3 -> 4;4;0 => 3;3;9)" <| fun _ ->
            let initPwd = largestPwd [0;0;2;4;4;0]
            Expect.equal initPwd [0;0;2;3;9;9] "Initial pwd list not correct."
            
        testCase "Applies rule one (last 4 -> 2;0;4;9 => 1;9;9;9)" <| fun _ ->
            let initPwd = largestPwd [0;0;2;0;4;9]
            Expect.equal initPwd [0;0;1;9;9;9] "Initial pwd list not correct."
        
        testCase "Applies rule one (all 1;0;2;0;4;9 to 0;9;9;9;9;9)" <| fun _ ->
            let initPwd = largestPwd [1;0;2;0;4;9]
            Expect.equal initPwd [0;9;9;9;9;9] "Initial pwd list not correct."
            
        testCase "Applies rule one (all 0;9;9;9;9;8 => 0;8;9;9;9;9)" <| fun _ ->
            let initPwd = largestPwd [0;9;9;9;9;8]
            Expect.equal initPwd [0;8;9;9;9;9] "Initial pwd list not correct."
            
        testCase "Applies rule one (all )" <| fun _ ->
            let initPwd = largestPwd [0;1;8;9;9;1]
            Expect.equal initPwd [0;1;8;8;9;9] "Initial pwd list not correct."

        testCase "Return some pwds within range" <| fun _ ->
            let pwd = decrementLastInt [0;0;0;0;6;9] 
            Expect.equal pwd [0;0;0;0;6;8] "Did not decrement the last int"
  ]
