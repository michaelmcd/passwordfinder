﻿module PasswordFinder
    //open System.Collections.Generic

    let toIntArray (input:string) =
        input 
        |> Seq.map (string >> int)
        |> Seq.toList
 
    let prepend0s count (list:int List) =
        let zeroCount = count - list.Length
        let zeros = List.init zeroCount (fun i -> 0)
        zeros @ list

    let contains2ConsecutiveNumbers nums =
            nums
            |> Seq.pairwise 
            |> Seq.filter (fun (a, b) -> if a = b then true else false)
            |> (not << Seq.isEmpty)
      
    let largestPwd numbers =
        let revNumbers = numbers |> List.rev
        let rec loop pwdNums nums =
            match nums with
            | fst::snd::rest -> 
                if fst < snd then
                    let transformed = pwdNums 
                                      |> List.map (fun p -> 9) 
                                      |> List.append [9]
                    loop (transformed) ((snd-1)::rest) 
                else    
                    loop (pwdNums @ [fst]) (snd::rest)
            | _ ->  pwdNums @ nums
        loop [] revNumbers |> List.rev

    let decrementLastInt (range:int List) = 
        let pwdCount = range.Length + (-1)
        range |> List.mapi (fun i v -> if i = pwdCount then (v + (-1)) else v)

    let rec findPwds pwds range =
        let pwd = largestPwd range      
        match pwd with 
        | [0;0;0;0;0;0] -> [pwd]::pwds
        | pwd -> let nextPwd = decrementLastInt pwd 
                 if contains2ConsecutiveNumbers pwd then
                    findPwds ([pwd]::pwds) nextPwd
                 else findPwds pwds nextPwd
        
        

    

    
        