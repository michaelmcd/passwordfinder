﻿open PasswordFinder

[<EntryPoint>]
let main argv =

    findPwds [] [1;1;0;0;1;2]
    |> List.iter (fun pwd -> printfn "\t %A" pwd)
    0
     